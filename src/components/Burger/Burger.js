import React from 'react';
import './Burger.css';
import Ingredient from "./Ingredient/Ingredient";

const Burger = ({ingredients}) => { // {salad: 2, bacon: 1, cheese: 1, meat: 2}
  const ingredientKeys = Object.keys(ingredients); // ["salad", "bacon", "cheese", "meat"]
  let ingList = [];

  ingredientKeys.forEach(igKey => {  // "salad"
    const amount = ingredients[igKey];  // 2
    for (let i = 0; i < amount; i++) { // 0 1
      ingList.push(<Ingredient key={igKey + i} type={igKey} />);
    }
  });

  if (ingList.length === 0) {
    ingList = <p>Please start adding ingredients!</p>;
  }

  return (
    <div className="Burger">
      <Ingredient type="bread-top"/>
      {ingList}
      <Ingredient type="bread-bottom"/>
    </div>
  )
};

export default Burger;