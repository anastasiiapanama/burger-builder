import axios from 'axios';

const axiosOrders = axios.create({
  baseURL: 'https://js9-burger-default-rtdb.firebaseio.com/'
});

export default axiosOrders;